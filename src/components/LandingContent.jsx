import { useState, useEffect } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import { contentCreate, contentGetAll } from '../APICall';
import { ToastContainer, toast } from 'react-toastify';
import ContentCard from './ContentCard';
import { useHistory } from 'react-router-dom';

function LandingContent() {
  const history = useHistory();

  const data = localStorage.getItem('accData') || null;
  const accRole = data != null ? JSON.parse(data).role : '2';

  const [show, setShow] = useState(false);
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [imageLink, setImageLink] = useState('');
  const [posts, setPosts] = useState([]);

  const handleClose = () => setShow(false);

  useEffect(() => {
    async function getPosts() {
      await contentGetAll().then((res) => {
        setPosts(res.data);
      });
    }
    getPosts();
  }, []);

  const handleSubmit = async (e, title, content, imageLink) => {
    e.preventDefault();
    await contentCreate(JSON.parse(data).id, {
      titlePost: title,
      contentPost: content,
      publishedTime: Date.now(),
      imageLink: imageLink,
    })
      .then(() => {
        toast.success('Success creating post.');
        setTimeout(() => {
          history.go(0);
        }, 2000);
      })
      .catch(() => toast.error('Failed creating post.'));
  };

  return (
    <div className="bg-dark">
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Creat New Post</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Title: </Form.Label>
              <Form.Control
                type="text"
                placeholder="Post Title"
                required
                onChange={(e) => setTitle(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Content: </Form.Label>
              <Form.Control
                type="text"
                placeholder="Post Content"
                required
                onChange={(e) => setContent(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Image Link: </Form.Label>
              <Form.Control
                type="text"
                placeholder="Leave empty for no image"
                onChange={(e) => setImageLink(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            type="submit"
            onClick={(e) => handleSubmit(e, title, content, imageLink)}
          >
            Create
          </Button>
        </Modal.Footer>
      </Modal>
      <div className="d-flex justify-content-end p-3">
        {accRole == '0' && <Button onClick={() => setShow(true)}>Create Post</Button>}
      </div>
      <div
        className="d-flex flex-wrap justify-content-center bg-dark"
        style={{ minHeight: '85vh' }}
      >
        {posts.map((post) => (
          <ContentCard
            key={post.id}
            title={post.titlePost}
            time={post.publishedTime}
            ids={post.idPost}
            totalComment={post.listOfComment.length}
            link={post.imageLink}
          />
        ))}
      </div>
    </div>
  );
}

export default LandingContent;
