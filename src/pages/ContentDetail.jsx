import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Layout from '../components/Layout';
import Loading from '../components/Loading';
import { contentGetById, contentDelete, contentEdit, commentAdd } from '../APICall';
import { AiFillDelete, AiFillEdit } from 'react-icons/ai';
import { RiSendPlane2Fill } from 'react-icons/ri';
import { ToastContainer, toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import { Button, Modal, Form } from 'react-bootstrap';
import CommentBubble from '../components/CommentBubble';

function ContentDetail() {
  const history = useHistory();
  const { id } = useParams();
  const [data, setData] = useState('');
  const [finish, setFinish] = useState(false);
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [imageLink, setImageLink] = useState('');
  const [show, setShow] = useState(false);
  const [comment, setComment] = useState('');
  const [comments, setComments] = useState([]);

  const acc = localStorage.getItem('accData') || null;

  useEffect(() => {
    async function fetchData() {
      await contentGetById(id).then((res) => {
        setData(res.data);
        setTitle(res.data.titlePost);
        setContent(res.data.contentPost);
        setImageLink(res.data.imageLink);
        setComments(res.data.listOfComment);
        setFinish(true);
      });
    }
    fetchData();
  }, [id]);

  const deleteHandler = async (acc, id) => {
    await contentDelete(JSON.parse(acc).id, id)
      .then(() => {
        toast.success('Succes deleting post.');
        setTimeout(() => {
          history.push('/');
        }, 2000);
      })
      .catch(() => {
        toast.error('Fail deletin post.');
      });
  };

  const handleSubmit = async (e, title, content, link, acc, id) => {
    e.preventDefault();
    await contentEdit(JSON.parse(acc).id, id, {
      titlePost: title,
      contentPost: content,
      publishedTime: data.publishedTime,
      imageLink: link,
    })
      .then(() => {
        toast.success('Succes editing post');
        setTimeout(() => {
          history.go(0);
        }, 2000);
      })
      .catch(() => {
        toast.error('Fail editing post');
      });
  };

  const commentHandler = async (e, comment, acc, id) => {
    e.preventDefault();
    await commentAdd(JSON.parse(acc).id, id, {
      isiComment: comment,
      publishedTime: Date.now(),
    })
      .then(() => {
        toast.success('Succes creating comnment.');
        setTimeout(() => {
          history.go(0);
        }, 2000);
      })
      .catch(() => toast.error('Failed creating comment.'));
  };

  const handleClose = () => setShow(false);

  if (!finish) {
    return <Loading />;
  }

  return (
    <Layout>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Post</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Title: </Form.Label>
              <Form.Control
                type="text"
                placeholder="Post Title"
                value={title}
                required
                onChange={(e) => setTitle(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Content: </Form.Label>
              <Form.Control
                type="text"
                placeholder="Post Content"
                value={content}
                required
                onChange={(e) => setContent(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Image Link: </Form.Label>
              <Form.Control
                type="text"
                placeholder="Leave empty for no image"
                value={imageLink}
                onChange={(e) => setImageLink(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            type="submit"
            onClick={(e) => handleSubmit(e, title, content, imageLink, acc, id)}
          >
            Edit
          </Button>
        </Modal.Footer>
      </Modal>
      <div style={{ minHeight: '86vh' }} className="bg-dark d-flex justify-content-center">
        <div
          style={{
            width: '70vh',
            minHeight: '80vh',
            borderWidth: '5px',
            borderStyle: 'solid',
            borderColor: 'wheat',
            borderRadius: '20px',
          }}
          className="bg-dark d-flex flex-column"
        >
          <div className="text-center" style={{ color: 'white', fontSize: '50px' }}>
            <div>{data.titlePost}</div>
            <div className="d-flex justify-content-end">
              {acc !== null && JSON.parse(acc).role === '0' && (
                <>
                  <div
                    className="d-flex px-3"
                    style={{ width: '60px', cursor: 'pointer' }}
                    onClick={() => setShow(true)}
                  >
                    <AiFillEdit />
                  </div>
                  <div
                    className="d-flex px-3"
                    style={{ width: '60px', cursor: 'pointer' }}
                    onClick={() => deleteHandler(acc, id)}
                  >
                    <AiFillDelete />
                  </div>
                </>
              )}
            </div>
          </div>
          <div
            className="text-center p-3"
            style={{ color: 'white', fontSize: '20px', minHeight: '70vh' }}
          >
            <p>{data.contentPost}</p>
          </div>
          <div className="d-flex p-3" style={{ color: 'white' }}>
            <div>
              Last update:
              <span style={{ color: 'blue', fontSize: '15px' }}>{data.updatedTime}</span>
            </div>
            <div className="ml-auto">
              Published at:
              <span style={{ color: 'blue', fontSize: '15px' }}>{data.publishedTime}</span>
            </div>
          </div>
        </div>
        <div
          style={{
            width: '50vh',
            marginLeft: '5vh',
            borderRadius: '20px',
            height: '50vh',
            borderWidth: '5px',
            borderStyle: 'solid',
            borderColor: 'wheat',
          }}
          className="d-flex flex-column p-3"
        >
          <div style={{ width: '100%', height: '85%', overflowX: 'scroll' }} className="p-2">
            {comments.map((cm) => (
              <CommentBubble comment={cm.isiComment} key={cm.idComment} cId={cm.idComment} />
            ))}
          </div>
          {acc !== null && (
            <div style={{ width: '100%', height: '15%', marginTop: '15px' }} className="d-flex p-2">
              <div
                style={{ background: 'white', width: '80%', borderRadius: '20px' }}
                className="d-flex justify-content-center align-items-center"
              >
                <input
                  type="text"
                  required
                  style={{ width: '100%', height: '100%', borderRadius: '20px', color: 'black' }}
                  onChange={(e) => setComment(e.target.value)}
                />
              </div>
              <div
                style={{ width: '13%', borderRadius: '50%', cursor: 'pointer' }}
                className="ml-auto bg-primary d-flex justify-content-center align-items-center"
                onClick={(e) => commentHandler(e, comment, acc, id)}
              >
                <RiSendPlane2Fill />
              </div>
            </div>
          )}
        </div>
      </div>
    </Layout>
  );
}

export default ContentDetail;
